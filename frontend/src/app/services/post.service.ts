import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { UpdatePost } from '../models/post/update-post';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Reaction[]>(`${this.routePrefix}/like`, reaction);
    }

    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Reaction[]>(`${this.routePrefix}/dislike`, reaction);
    }

    public updatePost(post: UpdatePost) {
        return this.httpService.putFullRequest<UpdatePost>(`${this.routePrefix}`, post);
    }

    public deletePost(postId: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${postId}`);
    }
}
