import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { UpdatePost } from 'src/app/models/post/update-post';

@Component({
    selector: 'edit-post-dialog',
    templateUrl: './edit-post-dialog.component.html',
    styleUrls: ['./edit-post-dialog.component.sass']
})
export class EditPostDialogComponent implements OnInit, OnDestroy {

    public post = {} as UpdatePost;
    public body: string;

    public imageFile: File;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<EditPostDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private postService: PostService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnInit() {
        this.post.id = this.data.post.id;
        this.post.body = this.data.post.body;
        this.post.previewImage = this.data.post.previewImage;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.post.previewImage = undefined;
        this.imageFile = undefined;
    }

    public saveChanges() {
        this.postService.updatePost(this.post)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.dialogRef.close(response),
                (error) => this.snackBarService.showErrorMessage(error));
    }

}
