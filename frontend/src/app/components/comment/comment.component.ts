import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { MainThreadComponent } from '../main-thread/main-thread.component';
import { Subject, Observable, empty } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { Post } from 'src/app/models/post/post';
import { UpdatePost } from 'src/app/models/post/update-post';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { LikesDislikesDialogComponent } from '../likes-dislikes-dialog/likes-dislikes-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public post: Post;

    private unsubscribe$ = new Subject<void>();
    public showEdit = false;
    private updatedComment = {} as UpdatePost;

    public constructor(
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private authDialogService: AuthDialogService,
        private authService: AuthenticationService,
        private dialog: MatDialog
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.post.comments.filter(comment => comment.id != this.comment.id);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public updateComment() {
        this.showEdit = true;
    }

    public closeEdit() {
        this.showEdit = false;
    }

    public isDeletable() {

        if (this.currentUser == null)
            return false;

        if (this.comment.author.id == this.currentUser.id)
            return true;

        if (this.post.author.id == this.currentUser.id)
            return true;

        return false;
    }

    public isEditable() {

        if (this.currentUser == null)
            return false;

        if (this.comment.author.id == this.currentUser.id) {
            return true;
        }

        return false;
    }

    public saveChanges() {
        this.updatedComment.id = this.comment.id;
        this.updatedComment.body = this.comment.body;

        this.commentService.updateComment(this.updatedComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.showEdit = false,
                (error) => this.snackBarService.showErrorMessage(error));
    }

    public getLikesCount() {
        let result = this.comment.reactions.filter(x => x.isLike === true);
        return result.length;
    }

    public getDislikesCount() {
        let result = this.comment.reactions.filter(x => x.isDislike === true);
        return result.length;
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((reactions) => (this.comment.reactions = reactions.body));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((reactions) => (this.comment.reactions = reactions.body));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((reactions) => (this.comment.reactions = reactions.body));

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((reactions) => (this.comment.reactions = reactions.body));
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public lookLikers() {
        // const dialog = this.dialog.open(LikesDislikesDialogComponent, {
        //     data: { post: this.comment },
        //     minWidth: 400,
        //     maxWidth: 600,
        //     autoFocus: true,
        //     backdropClass: 'dialog-backdrop',
        //     position: {
        //         top: '0'
        //     }
        // });

        // dialog.afterClosed()
        //     .pipe(takeUntil(this.unsubscribe$));
    }
}
