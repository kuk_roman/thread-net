import { Component, OnInit, Inject, OnDestroy, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post/post';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
    selector: 'likes-dislikes-dialog',
    templateUrl: './likes-dislikes-dialog.component.html',
    styleUrls: ['./likes-dislikes-dialog.component.sass']
})
export class LikesDislikesDialogComponent implements OnInit, OnDestroy {
    public post: Post;
    public usersLikes: User[] = [];
    public usersDislikes: User[] = [];

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<LikesDislikesDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) { }

    public ngOnInit() {
        this.post = this.data.post;
        let reactions = this.post.reactions.filter(x => x.isLike === true);
        for (let x of reactions) {
            this.usersLikes.push(x.user);
        }
        reactions = this.post.reactions.filter(x => x.isDislike === true);
        for (let x of reactions) {
            this.usersDislikes.push(x.user);
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    // public setChoice(x: string) {
    //     this.users = [];
    //     this.choice = x;
    //     if (x === "likes")
    //         this.post.reactions.filter(x => x.isLike === true).forEach(function (x) { this.users.push(x.user) });
    //     else
    //         this.post.reactions.filter(x => x.isLike === true).forEach(function (x) { this.users.push(x.user) });
    // }
}
