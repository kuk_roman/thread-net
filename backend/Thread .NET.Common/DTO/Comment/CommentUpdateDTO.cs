﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentUpdateDTO
    {
        public int Id { get; set; }
        public string Body { get; set; }
    }
}
