﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<ReactionDTO>> LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId 
                && x.PostId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId
                && x.PostId == reaction.EntityId && x.IsDislike == true).ToList();


            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
            }
            else if (dislikes.Any())
            {
                dislikes.ForEach(like => { like.IsLike = true; like.IsDislike = false; like.UpdatedAt = DateTime.Now; });
                _context.PostReactions.UpdateRange(dislikes);
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    IsDislike = false,
                    UserId = reaction.UserId
                }); 
                
                await _context.SaveChangesAsync();
            }

            var reactions = await _context.PostReactions
                .Include(x => x.User)
                    .ThenInclude(user => user.Avatar)
                .Where(x => x.PostId == reaction.EntityId)
                .Select(x => _mapper.Map<ReactionDTO>(x)).ToListAsync();

            return reactions;
        }

        public async Task<ICollection<ReactionDTO>> DislikePost(NewReactionDTO reaction)
        {
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId
                && x.PostId == reaction.EntityId && x.IsDislike == true);
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId
                && x.PostId == reaction.EntityId && x.IsLike == true).ToList();

            if (dislikes.Any())
            {
                _context.PostReactions.RemoveRange(dislikes);
                await _context.SaveChangesAsync();
            }
            else if (likes.Any())
            {
                likes.ForEach(like => { like.IsLike = false; like.IsDislike = true; like.UpdatedAt = DateTime.Now; });
                _context.PostReactions.UpdateRange(likes);
                await _context.SaveChangesAsync();
            }

            else
            {
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = false,
                    IsDislike = reaction.IsDislike,
                    UserId = reaction.UserId
                });

                await _context.SaveChangesAsync();
            }

            var reactions = await _context.PostReactions
                .Include(x => x.User)
                    .ThenInclude(user => user.Avatar)
                .Where(x => x.PostId == reaction.EntityId)
                .Select(x => _mapper.Map<ReactionDTO>(x)).ToListAsync();

            return reactions;
        }

        public async Task<ICollection<ReactionDTO>> LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId
                && x.CommentId == reaction.EntityId && x.IsLike == true);
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId
                && x.CommentId == reaction.EntityId && x.IsDislike == true).ToList();


            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();
            }
            else if (dislikes.Any())
            {
                dislikes.ForEach(like => { like.IsLike = true; like.IsDislike = false; like.UpdatedAt = DateTime.Now; });
                _context.CommentReactions.UpdateRange(dislikes);
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    IsDislike = false,
                    UserId = reaction.UserId
                });

                await _context.SaveChangesAsync();
            }

            var reactions = await _context.CommentReactions
                .Include(x => x.User)
                    .ThenInclude(user => user.Avatar)
                .Where(x => x.CommentId == reaction.EntityId)
                .Select(x => _mapper.Map<ReactionDTO>(x)).ToListAsync();

            return reactions;
        }

        public async Task<ICollection<ReactionDTO>> DislikeComment(NewReactionDTO reaction)
        {
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId
                && x.CommentId == reaction.EntityId && x.IsDislike == true);
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId
                && x.CommentId == reaction.EntityId && x.IsLike == true).ToList();

            if (dislikes.Any())
            {
                _context.CommentReactions.RemoveRange(dislikes);
                await _context.SaveChangesAsync();
            }
            else if (likes.Any())
            {
                likes.ForEach(like => { like.IsLike = false; like.IsDislike = true; like.UpdatedAt = DateTime.Now; });
                _context.CommentReactions.UpdateRange(likes);
                await _context.SaveChangesAsync();
            }

            else
            {
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = false,
                    IsDislike = reaction.IsDislike,
                    UserId = reaction.UserId
                });

                await _context.SaveChangesAsync();
            }

            var reactions = await _context.CommentReactions
                .Include(x => x.User)
                    .ThenInclude(user => user.Avatar)
                .Where(x => x.CommentId == reaction.EntityId)
                .Select(x => _mapper.Map<ReactionDTO>(x)).ToListAsync();

            return reactions;
        }
    }
}
