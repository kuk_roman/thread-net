﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task DeleteComment(int commentId)
        {
            var commentEntity = await _context.Comments.FirstOrDefaultAsync(u => u.Id == commentId);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentId);
            }

            var commentReactions = _context.CommentReactions.Where(commentReaction => 
                    commentReaction.CommentId == commentId).ToList();
            commentReactions.ForEach(commentReaction => _context.CommentReactions.Remove(commentReaction));

            _context.Comments.Remove(commentEntity);
            await _context.SaveChangesAsync();
        }

        public async Task<CommentDTO> UpdateComment(CommentUpdateDTO commentDto)
        {
            var commentEntity = await GetCommentByIdInternal(commentDto.Id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Post), commentDto.Id);
            }

            commentEntity.Body = commentDto.Body;

            var timeNow = DateTime.Now;

            commentEntity.UpdatedAt = timeNow;

            var updatedComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(comment => comment.Reactions)
                    .ThenInclude(reaction => reaction.User)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            _context.Comments.Update(updatedComment);
            await _context.SaveChangesAsync();

            var updatedCommentDTO = _mapper.Map<CommentDTO>(updatedComment);
            //await _postHub.Clients.All.SendAsync("UpdatedPost", updatedCommentDTO);

            return updatedCommentDTO;
        }

        private async Task<Comment> GetCommentByIdInternal(int id)
        {
            return await _context.Comments
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
